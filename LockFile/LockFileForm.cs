﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LockFile
{
    public partial class LockFileForm : Form, ILockFileView
    {
        private LockFilePresenter presenter;

        public LockFileForm()
        {
            presenter = new LockFilePresenter(this);
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    presenter.SetFile(dlg.FileName);
                }
            }
        }

        private void btnLock_Click(object sender, EventArgs e)
        {
            presenter.LockFile();
        }

        private void btnUnlock_Click(object sender, EventArgs e)
        {
            presenter.UnlockFile();
        }

        #region ILockFileView Members

        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }

        public void SetStatus(LockStatus lockStatus)
        {
            lblUnlocked.Visible = (lockStatus == LockStatus.Unlocked);
            lblLocked.Visible = (lockStatus == LockStatus.Locked);
        }

        public void SetFile(string path)
        {
            txtFile.Text = path;
        }

        #endregion
    }
}
