﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LockFile
{
    public class LockFileConsole : ILockFileView
    {
        public void ShowMessage(string p)
        {
            Console.WriteLine(p);
        }

        public void SetStatus(LockStatus lockStatus)
        {
            Console.WriteLine("Lock Status: {0}", lockStatus);
        }

        public void SetFile(string path)
        {
            Console.WriteLine("Lock File: {0}", path);
        }
    }
}
