﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace LockFile
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new LockFileForm());
            }
            else
            {
                ConsoleHelper.AttachOrAllocConsole();
                LockFileConsole console = new LockFileConsole();
                LockFilePresenter presenter = new LockFilePresenter(console);
                presenter.SetFile(args[0]);
                Console.Clear();

                while (true)
                {
                    Console.Clear();
                    presenter.ShowStatus();
                    Console.WriteLine();
                    Console.WriteLine("Press enter to lock the file (Ctrl+C to exit).");
                    Console.ReadLine();
                    Console.Clear();

                    if (!presenter.LockFile())
                        break;

                    Console.Clear();
                    presenter.ShowStatus();
                    Console.WriteLine();
                    Console.WriteLine("Press enter to unlock the file (Ctrl+C to exit).");
                    Console.ReadLine();
                    Console.Clear();

                    if (!presenter.UnlockFile())
                        break;
                }

                Console.WriteLine();
                Console.WriteLine("Press enter to exit.");
                Console.ReadLine();
                ConsoleHelper.FreeConsole();
            }
        }
    }
}
