﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace LockFile
{
    public enum LockStatus
    {
        Unlocked,
        Locked
    }

    public class LockFilePresenter
    {
        private ILockFileView view;
        private string file2Lock = string.Empty;
        private FileStream fileLockStream = null;

        public LockFilePresenter(ILockFileView view)
        {
            this.view = view;
        }

        internal bool LockFile()
        {
            if (string.IsNullOrEmpty(file2Lock) || !File.Exists(file2Lock))
            {
                view.ShowMessage("Please select a path to lock.");
                return false;
            }

            if (fileLockStream != null)
            {
                view.ShowMessage("The path is already locked.");
                return false;
            }
            try
            {
                fileLockStream = File.Open(file2Lock, FileMode.Open);
                fileLockStream.Lock(0, fileLockStream.Length);
                view.SetStatus(LockStatus.Locked);
                return true;
            }
            catch (Exception ex)
            {
                fileLockStream = null;
                view.SetStatus(LockStatus.Unlocked);
                view.ShowMessage(string.Format("An error occurred locking the path.\r\n\r\n{0}", ex.Message));
                return false;
            }
        }

        internal bool UnlockFile()
        {
            if (fileLockStream == null)
            {
                view.ShowMessage("No path is currently locked.");
                return false;
            }
            try
            {
                using (fileLockStream)
                    fileLockStream.Unlock(0, fileLockStream.Length);
                return true;
            }
            catch (Exception ex)
            {
                view.ShowMessage(string.Format("An error occurred unlocking the path.\r\n\r\n{0}", ex.Message));
                return false;
            }
            finally
            {
                fileLockStream = null;
                view.SetStatus(LockStatus.Unlocked);
            }
        }

        internal void SetFile(string path)
        {
            if (ValidateFile(path))
            {
                if (fileLockStream != null)
                    UnlockFile();
                file2Lock = path;
                view.SetFile(path);
                view.SetStatus(LockStatus.Unlocked);
            }
        }

        internal bool ValidateFile(string path)
        {
            bool exists = File.Exists(path);
            if (!exists)
                view.ShowMessage("File does not exist.");
            return exists;
        }

        internal void ShowStatus()
        {
            view.SetFile(file2Lock);
            view.SetStatus(fileLockStream == null ? LockStatus.Unlocked : LockStatus.Locked);
        }
    }
}
