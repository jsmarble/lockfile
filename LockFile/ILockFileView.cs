﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LockFile
{
    public interface ILockFileView
    {
        void ShowMessage(string p);
        void SetStatus(LockStatus lockStatus);

        void SetFile(string path);
    }
}
